import { type Plugin } from "vite";
import prepr from "prepr";
import wabtModule from "wabt";
import binaryen from "binaryen";

interface Option {
  forceOptimize?: boolean;
  enableDebugLog?: boolean;
}

function appendDefines(code: string, mode: string, enableDebugLog: boolean) {
  const codeArr = code.split("\n");
  if (mode !== "") {
    codeArr.unshift(`#define ${mode}`);
  }
  if (enableDebugLog) {
    const moduleIdx = codeArr.findIndex((line) => line.includes("module"));
    if (moduleIdx > 0) {
      codeArr.splice(
        moduleIdx + 1,
        0,
        '(import "debug" "log" (func $debugLog (param i32 i32)))'
      );
      codeArr.unshift(
        "#define __DEBUGLOG__(code, value) (call $debugLog (i32.const code)(value))"
      );
    }
  } else {
    codeArr.unshift(
      "#define __DEBUGLOG__(code, value) (;do nothing (code)(value);)"
    );
  }
  return codeArr.join("\n");
}

export default function wat2wasm({
  forceOptimize = false,
  enableDebugLog = false,
}: Option = {}): Plugin {
  const watReg = /\.wat/;
  let optimize = false;
  let command = "";
  return {
    name: "wat2wasm",

    configResolved(config) {
      optimize = config.command === "build" || forceOptimize;
      command = config.command.toUpperCase();
    },

    async transform(code, id) {
      if (!watReg.test(id)) return;

      code = appendDefines(code, command, enableDebugLog);

      try {
        code = prepr(code);
        const wabt = await wabtModule();
        const wasmModule = wabt.parseWat("", code);
        wasmModule.validate();
        const binRes = wasmModule.toBinary({
          write_debug_names: true,
        });
        let wasm = binRes.buffer;
        wasmModule.destroy();
        if (optimize) {
          const mod = binaryen.readBinary(wasm);
          mod.setFeatures(binaryen.Features.BulkMemory);
          mod.optimize();
          wasm = mod.emitBinary();
          mod.dispose();
        }
        const base64 = btoa(
          Array.from(wasm, (byte: number) => String.fromCharCode(byte)).join("")
        );

        const name = id.split("/").reverse()[0].split("?")[0];
        this.info(
          `${name}: ${(wasm.byteLength / 1000).toFixed(3)} kB (base64: ${(
            base64.length / 1000
          ).toFixed(3)} kB)`
        );

        const moduleWrapperCode = [
          "export default function moduleWrapper(opts={}){",
          enableDebugLog
            ? "opts={debug:{log(a,b){console.log(`" +
              name +
              ":${performance.now().toFixed(2)}:code${a}:${b}`)}},...opts};"
            : "",
          `const b=Uint8Array.from(atob("${base64}"),c=>c.charCodeAt(0));`,
          id.endsWith("?promise")
            ? `return WebAssembly.instantiate(b,opts).then(o=>o.instance);`
            : `return new WebAssembly.Instance(new WebAssembly.Module(b),opts);`,
          "}",
        ].join("");

        return {
          code: moduleWrapperCode,
          map: null,
        };
      } catch (e) {
        return this.error(e);
      }
    },
  } as Plugin;
}
