import {default as GIF} from "../src/gifenc.ts";

const bitDepth = 8;
const applyDither = true;
const delayTick: number = 2;

const frames: number = delayTick !== 0 ? 100 / delayTick : 60;

navigator.mediaDevices
  .getUserMedia({ video: true, audio: false })
  .then((stream) => {
    const camElm = document.querySelector<HTMLVideoElement>("#cam")!;
    const btnElm = document.querySelector<HTMLButtonElement>("#rec")!;
    const msg1Elm = document.querySelector<HTMLDivElement>("#msg1")!;
    const msg2Elm = document.querySelector<HTMLDivElement>("#msg2")!;

    camElm.srcObject = stream;
    camElm.play();
    btnElm.style.display = "inline-block";
    let url: string | null = null;

    btnElm.onclick = () => {
      btnElm.style.display = "none";
      const canvas = new OffscreenCanvas(
        camElm.clientWidth,
        camElm.clientHeight
      );
      const ctx = canvas.getContext("2d", { willReadFrequently: true })!;
      const gif = new GIF(canvas.width, canvas.height, bitDepth, 4);
      let start = 0;
      const frameData: ImageData[] = [];

      const id = setInterval(
        () => {
          if (frameData.length < frames) {
            ctx.drawImage(camElm, 0, 0, canvas.width, canvas.height);
            const frame = ctx.getImageData(0, 0, canvas.width, canvas.height);
            frameData.push(frame);
            msg1Elm.innerText = `rec ${frameData.length}/${frames} frames`;
          } else {
            clearTimeout(id);
            start = performance.now();
            setTimeout(encodeFrame, 0);
          }
        },
        delayTick !== 0 ? delayTick * 10 : 17
      );

      const encodeFrame = () => {
        if (frameData.length > 0) {
          gif.pushImg(frameData.shift()!, delayTick, {applyDither}).then((ret) => {
            msg2Elm.innerText = `encode ${ret.numProcessed}/${frames} frames`;
          });
          msg1Elm.innerText = `push ${
            frames - frameData.length
          }/${frames} frames`;
          setTimeout(encodeFrame, 0);
        } else {
          gif.finish().then((data) => {
            const time = performance.now() - start;
            const imgElm = document.querySelector<HTMLImageElement>("#result")!;
            imgElm.width = canvas.width;
            imgElm.height = canvas.height;
            if (url) URL.revokeObjectURL(url);
            url = URL.createObjectURL(new Blob([data], { type: "image/gif" }));
            imgElm.src = url;
            msg1Elm.innerText = `encoding took roughly ${time.toFixed(2)}ms `;
            msg2Elm.innerText = `${1 << bitDepth}colors ${canvas.width}x${
              canvas.height
            }pixel ${frames}frames`;
            btnElm.style.display = "inline-block";
            gif.dispose();
          });
        }
      };
    };
  })
  .catch((e) => {
    console.log(e);
  });
