import {default as GIF} from "../src/gifenc.ts";

const bitDepth: number = 8;
const size: number = 512;
const delayTick: number = 2;

const frames: number = delayTick !== 0 ? 100 / delayTick : 60;
const palette = Uint8Array.from({ length: (1 << bitDepth) * 3 }, () =>
  Math.min(255, Math.round(Math.random() * 255))
);
const paletteSize = 1 << bitDepth;
const msg1Elm = document.querySelector<HTMLImageElement>("#msg1")!;
const msg2Elm = document.querySelector<HTMLImageElement>("#msg2")!;
const gif = new GIF(size, size, bitDepth, 4, palette);

let start = 0;
const frameData: ArrayBuffer[] = [];

const genFrame = () => {
  if (frameData.length < frames) {
    const frame = Uint8Array.from({ length: size * size }, () =>
      Math.min(paletteSize - 1, Math.round(Math.random() * paletteSize))
    ).buffer;
    frameData.push(frame);
    msg1Elm.innerText = `gen ${frameData.length}/${frames} frames`;
    setTimeout(genFrame, 0);
  } else {
    start = performance.now();
    setTimeout(encodeFrame, 0);
  }
};

const encodeFrame = () => {
  if (frameData.length > 0) {
    gif.push(frameData.shift()!, size, delayTick).then((ret) => {
      msg2Elm.innerText = `encode ${ret.numProcessed}/${frames} frames`;
    });
    msg1Elm.innerText = `push ${frames - frameData.length}/${frames} frames`;
    setTimeout(encodeFrame, 0);
  } else {
    gif.finish().then((data) => {
      const time = performance.now() - start;
      const imgElm = document.querySelector<HTMLImageElement>("#result")!;
      const scale = devicePixelRatio;
      imgElm.style.width = `${Math.floor(size / scale)}px`;
      imgElm.style.height = `${Math.floor(size / scale)}px`;
      imgElm.width = size;
      imgElm.height = size;

      const url = URL.createObjectURL(new Blob([data], { type: "image/gif" }));
      imgElm.src = url;
      msg1Elm.innerText = `encoding took roughly ${time.toFixed(2)}ms `;
      msg2Elm.innerText = `${paletteSize}colors ${size}x${size}pixel ${frames}frames`;
    });
  }
};

setTimeout(genFrame, 0);
