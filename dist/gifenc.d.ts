/**
 * see gif spec
 */
type DisposalMethod = "none" | "not" | "background" | "previous";
interface OptionBase {
    /**
     * transferring data to workers
     * @default true
     */
    transfer?: boolean;
    /**
     * see gif spec
     * @default 0
     */
    xOffset?: number;
    /**
     * see gif spec
     * @default 0
     */
    yOffset?: number;
    /**
     * see gif spec
     * @default "none"
     */
    disposal?: DisposalMethod;
}
interface PushOption extends OptionBase {
    /**
     * frame local RGB palette
     * @default null
     */
    palette?: ArrayBuffer | null;
    /**
     * see gif spec
     * @default null
     */
    transparentIndex?: number | null;
}
interface PushImgOption extends OptionBase {
    /**
     * hint indicating than num of colors used is less than palette size
     * @default false
     */
    mayFitOnPalette?: boolean;
    /**
     * transparent when alpha ch is less than 0x80
     * @default false
     */
    useTransparent?: boolean;
    /**
     * apply dithering
     * @default false
     */
    applyDither?: boolean;
    /**
     * RGB of background color
     * @default null
     */
    bgColor?: ArrayLike<number> | null;
}
interface PushResult {
    /**
     * processed frame num
     */
    frameNum: number;
    /**
     * total frames processed
     */
    numProcessed: number;
}
/**
 * gif encoder
 */
declare class Encoder {
    #private;
    /**
     *
     * @param width - gif image width
     * @param height - gif image height
     * @param bitDepth - palette size
     * @param workerCount - num of workers use
     * @param globalPalette - global RGB palette (default: null)
     */
    constructor(width: number, height: number, bitDepth: number, workerCount: number, globalPalette?: ArrayBuffer | null);
    /**
     * width
     */
    get width(): number;
    /**
     * height
     */
    get height(): number;
    /**
     * bit depth
     */
    get bitDepth(): number;
    /**
     * palette size
     */
    get paletteSize(): number;
    /**
     * push indices
     * @param indices - indices array (1index per byte)
     * @param width - image width
     * @param delayTick - delay time (1tick = 1/100sec)
     * @param opts - options
     * @returns - processed frame num and total frames processed
     */
    push(indices: ArrayBuffer, width: number, delayTick: number, { transfer, xOffset, yOffset, disposal, palette, transparentIndex, }?: PushOption): Promise<PushResult>;
    /**
     * push image data
     * @param img - image data
     * @param delayTick - delay time (1tick = 1/100sec)
     * @param opts - options
     * @returns - processed frame num and total frames processed
     */
    pushImg(img: ImageData, delayTick: number, { transfer, xOffset, yOffset, disposal, mayFitOnPalette, useTransparent, applyDither, bgColor, }?: PushImgOption): Promise<PushResult>;
    /**
     * generate gif image
     * @param loop - loop count (if 0 then endless) (default: 0)
     * @returns - gif image blob
     */
    finish(loop?: number): Promise<Blob>;
    /**
     * dispose instance
     */
    dispose(): void;
}
export default Encoder;
export type { PushOption, PushImgOption, PushResult };
