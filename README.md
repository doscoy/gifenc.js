# gifenc.js

そこそこ速いgifエンコーダ

## build

```sh
npm install
npm run build
```

## usage

使用にあたってgifの仕様を把握しておくとわかりやすいかもしれない

```js
import Encoder from "gifenc.js"

const globalPalette = new Uint8Array([
  0xff, 0x00, 0x00, // index 0 rgb(255, 0, 0)
  0x00, 0xff, 0x00, // index 1 rgb(0, 255, 0)
  ...
  0x00, 0x00, 0xff, // index 255 rgb(0, 0, 255)
]).buffer;
const gif = new Encoder(
  256,           // width
  256,           // height
  8,             // bit depth
  4,             // num of worker
  globalPalette  // global palette
);

const indices = new Uint8Array(...).buffer;
gif.push(
  indices, // imgae data
  256,     // width
  2,       // delay tick
);

const img = new ImageData(...);
gif.pushImg(
  img, // imgae data
  2,   // delay tick
);

gif.finish().then((blob) => {
  console.log(blob.type) // "image/gif"
});

```

## example

```sh
npm install
npm run dev
```
- http://localhost:{port}/example/randidx.html
  - ランダムなグローバルパレットとランダムなインデックスからgifアニメ生成
- http://localhost:{port}/example/webcam.html
  - webカメラのキャプチャを減色してgifアニメ生成

## reference
- https://www.w3.org/Graphics/GIF/spec-gif89a.txt
- https://github.com/mattdesl/gifenc
- http://ndevilla.free.fr/median/median/index.html
- https://webassembly.github.io/spec/core/index.html
- https://developer.mozilla.org/en-US/docs/WebAssembly/Reference