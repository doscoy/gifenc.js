interface FrameControl {
  width: number;
  height: number;
  delayTick: number;
  xOffset: number;
  yOffset: number;
  palette: ArrayBuffer | null;
  disposal: number;
  bitDepth: number;
  transparentIndex: number | null;
}

class BufferWriter {
  #buffer: ArrayBuffer;
  #cursor: number;

  constructor(size: number) {
    this.#buffer = new ArrayBuffer(size);
    this.#cursor = 0;
  }

  get buffer(): ArrayBuffer {
    return this.#buffer;
  }

  writeNumber(val: number, bytes: 1 | 2 | 4): void {
    const view = new DataView(this.#buffer);
    switch (bytes) {
      case 1:
        view.setUint8(this.#cursor, val);
        break;
      case 2:
        view.setUint16(this.#cursor, val, true);
        break;
      case 4:
        view.setUint32(this.#cursor, val, true);
        break;
    }
    this.#cursor += bytes;
  }

  writeData(data: Uint8Array): void {
    const arr = new Uint8Array(this.#buffer, this.#cursor, data.length);
    arr.set(data);
    this.#cursor += data.length;
  }
}

const HEADER_DATA = new Uint8Array([
  0x47, 0x49, 0x46,
  0x38, 0x39, 0x61,
]);

function makeGlobalHeader(
  width: number,
  height: number,
  bitDepth: number,
  backgroundIndex: number,
  globalPalette: ArrayBuffer | null,
): ArrayBuffer  {
  const writer = new BufferWriter(
    13 + (globalPalette ? globalPalette.byteLength : 0));
  writer.writeData(HEADER_DATA);
  writer.writeNumber(width, 2);
  writer.writeNumber(height, 2);
  const fixedBits = (bitDepth - 1) & 0b111;
  if (globalPalette) {
    writer.writeNumber(0b10000000 | (fixedBits << 4) | fixedBits, 1);
  } else {
    writer.writeNumber(fixedBits << 4, 1);
  }
  writer.writeNumber(backgroundIndex, 1);
  writer.writeNumber(0, 1);
  if (globalPalette) {
    writer.writeData(new Uint8Array(globalPalette));
  }
  return writer.buffer;
}

const LOOP_CONTROL_DATA = new Uint8Array([
  0x21,
  0xff,
  0x0b,
  0x4e, 0x45, 0x54, 0x53, 0x43, 0x41, 0x50, 0x45,
  0x32, 0x2e, 0x30,
  0x03,
  0x01,
]);

function makeLoopControl(loop: number): ArrayBuffer {
  const writer = new BufferWriter(19);
  writer.writeData(LOOP_CONTROL_DATA);
  writer.writeNumber(loop, 2);
  writer.writeNumber(0x00, 1); // terminator
  return writer.buffer;
}

const GRAPHIC_CONTROL_EXTENSION_DATA = new Uint8Array([
  0x21,
  0xf9,
  0x04,
]);

function makeGraphicControlExtension(frame: FrameControl): ArrayBuffer {
  const writer = new BufferWriter(8);
  writer.writeData(GRAPHIC_CONTROL_EXTENSION_DATA);
  writer.writeNumber(
    (frame.disposal << 2) | (frame.transparentIndex ? 1 : 0),
    1,
  );
  writer.writeNumber(frame.delayTick, 2);
  writer.writeNumber(frame.transparentIndex ? frame.transparentIndex : 0, 1);
  writer.writeNumber(0x00, 1); // terminator
  return writer.buffer;
}

function makeImageBlockHeader(frame: FrameControl): ArrayBuffer {
  const writer = new BufferWriter(
    10 + (frame.palette ? frame.palette.byteLength : 0)
  );
  writer.writeNumber(0x2c, 1);
  writer.writeNumber(frame.xOffset, 2);
  writer.writeNumber(frame.yOffset, 2);
  writer.writeNumber(frame.width, 2);
  writer.writeNumber(frame.height, 2);
  if (frame.palette) {
    writer.writeNumber(0b10000000 | ((frame.bitDepth - 1) & 0b111), 1);
    writer.writeData(new Uint8Array(frame.palette));
  } else {
    writer.writeNumber(0, 1);
  }
  return writer.buffer;
}

export {makeGlobalHeader, makeLoopControl, makeGraphicControlExtension, makeImageBlockHeader}
export type {FrameControl}