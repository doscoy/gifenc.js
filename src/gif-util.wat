(module
  #define WORKMEMSIZE 65536
  #define MEMPAGESIZE 65536

  #define TREEADDR 1024

  #define MEDIANCUTSTACKADDR 5120

  #define SORTSTACKADDR 6144

  #define COUNTFLAG 1
  #define TRANSPARENTFLAG 2
  #define DITHERFLAG 4

  ;; COUNTFLAG | TRANSPARENTFLAG
  #define SORTFLAG 3

  (global (export "inputOffset") i32 (i32.const WORKMEMSIZE))
  (global (export "countFlag") i32 (i32.const COUNTFLAG))
  (global (export "transparentFlag") i32 (i32.const TRANSPARENTFLAG))
  (global (export "ditherFlag") i32 (i32.const DITHERFLAG))
  (global $paletteCursor (mut i32) (i32.const 0))
  (memory (export "memory") 2)

  (func (export "compress")(param $imgLength i32)(param $bitDepth i32)(result i32)
    (;
      /***************************************************************************
      *
      *  GIFENCOD.C       - GIF Image compression routines
      *
      *  Lempel-Ziv compression based on 'compress'.  GIF modifications by
      *  David Rowley (mgardi@watdcsu.waterloo.edu)
      *
      ***************************************************************************/

      /*
      *
      * GIF Image compression - modified 'compress'
      *
      * Based on: compress.c - File compression ala IEEE Computer, June 1984.
      *
      * By Authors:  Spencer W. Thomas       (decvax!harpo!utah-cs!utah-gr!thomas)
      *              Jim McKie               (decvax!mcvax!jim)
      *              Steve Davies            (decvax!vax135!petsd!peora!srd)
      *              Ken Turkowski           (decvax!decwrl!turtlevax!ken)
      *              James A. Woods          (decvax!ihnp4!ames!jaw)
      *              Joe Orost               (decvax!vax135!petsd!joe)
      *
      */

      /*
      * compress stdin to stdout
      *
      * Algorithm:  use open addressing double hashing (no chaining) on the
      * prefix code / next character combination.  We do a variant of Knuth's
      * algorithm D (vol. 3, sec. 6.4) along with G. Knott's relatively-prime
      * secondary probe.  Here, the modular division first probe is gives way
      * to a faster exclusive-or manipulation.  Also do block compression with
      * an adaptive reset, whereby the code table is cleared when the compression
      * ratio decreases, but after the table fills.  The variable-length output
      * codes are re-sized at this point, and a special CLEAR code is generated
      * for the decompressor.  Late addition:  construct the table according to
      * file size for noticeable speed improvement on small files.  Please direct
      * questions about this implementation to ames!jaw.
      */
    ;)
    (local $px i32)
    (local $initBits i32)
    (local $pageBits i32)
    (local $pageSize i32)
    (local $ent i32)
    (local $clearCode i32)
    (local $freeEnt i32)
    (local $outCur i32)
    (local $sizeCur i32)
    (local $blockCnt i32)
    (local $bitsCur i32)
    (local $accum i32)
    (local $i i32)
    (local $c i32)
    (local $fcode i64)
    (local $disp i32)

    #define MAXBITS 12
    #define MAXPAGESIZE 4096
    #define HSIZE 5003
    #define HSHIFT 4
    ;; HSIZE x 2 x 4
    #define HMEMSIZE 40024

    (local.set $px (i32.const 1))

    (if (i32.lt_s (local.get $bitDepth)(i32.const 2))
      (then (local.set $initBits (i32.const 3)))
      (else (local.set $initBits (i32.add (local.get $bitDepth)(i32.const 1)))))
    
    (local.set $pageBits (local.get $initBits))
    (local.set $pageSize (i32.sub (i32.shl (i32.const 1)(local.get $pageBits))(i32.const 1)))
    (local.set $ent (i32.load8_u (i32.const WORKMEMSIZE)))
    (local.set $clearCode (i32.shl (i32.const 1)(i32.sub (local.get $initBits)(i32.const 1))))
    (local.set $freeEnt (i32.add (local.get $clearCode)(i32.const 2)))
    (local.set $outCur (i32.add (i32.const WORKMEMSIZE)(local.get $imgLength)))
    
    (if (i32.ge_u (local.get $outCur)(i32.mul (memory.size)(i32.const MEMPAGESIZE))) (then
      (memory.grow (i32.const 1))
      drop
    ))

    (memory.fill (i32.const 0)(i32.const 255)(i32.const HMEMSIZE))

    (i32.store8 (local.get $outCur)(i32.sub (local.get $initBits)(i32.const 1)))

    (local.set $outCur (i32.add (local.get $outCur)(i32.const 1)))
    (local.set $sizeCur (local.get $outCur))
    (local.set $outCur (i32.add (local.get $outCur)(i32.const 1)))
    (local.set $accum (local.get $clearCode))
    (local.set $bitsCur (i32.add (local.get $bitsCur)(local.get $pageBits)))

    (block $eof (loop $nextPx (br_if $eof (i32.ge_s (local.get $px)(local.get $imgLength)))
      
      (local.set $c (i32.load8_u (i32.add (i32.const WORKMEMSIZE)(local.get $px))))
      (local.set $px (i32.add (local.get $px)(i32.const 1)))
      (local.set $fcode (i64.add (i64.shl (i64.extend_i32_s (local.get $c))(i64.const MAXBITS))(i64.extend_i32_s (local.get $ent))))
      (local.set $i (i32.xor (i32.shl (local.get $c)(i32.const HSHIFT))(local.get $ent)))
      (if (i64.eq (i64.extend_i32_s (i32.load align=4 (i32.mul (local.get $i)(i32.const 4))))(local.get $fcode)) (then
        (local.set $ent (i32.load align=4 (i32.mul (i32.add (i32.const HSIZE)(local.get $i))(i32.const 4))))
        br $nextPx
      ))

      (block $match
        (if (i32.lt_s (i32.load align=4 (i32.mul (local.get $i)(i32.const 4)))(i32.const 0)) (then
          br $match
        ))

        (local.set $disp (i32.sub (i32.const HSIZE)(local.get $i)))

        (if (i32.eqz (local.get $i)) (then
          (local.set $disp (i32.const 1))
        ))

        (block $exitProbe (loop $probe
          (local.set $i (i32.sub (local.get $i)(local.get $disp)))
          (if (i32.lt_s (local.get $i)(i32.const 0)) (then
            (local.set $i (i32.add (local.get $i)(i32.const HSIZE)))
          ))

          (if (i64.eq (i64.extend_i32_s (i32.load align=4 (i32.mul (local.get $i)(i32.const 4))))(local.get $fcode)) (then
            (local.set $ent (i32.load align=4 (i32.mul (i32.add (i32.const HSIZE)(local.get $i))(i32.const 4))))
            br $nextPx
          ))

          (if (i32.gt_s (i32.load align=4 (i32.mul (local.get $i)(i32.const 4)))(i32.const 0)) (then
            br $probe
          ) (else
            br $exitProbe
          ))

          br $match
        ))
      )

      (local.set $accum (i32.or (local.get $accum)(i32.shl (local.get $ent)(local.get $bitsCur))))
      (local.set $bitsCur (i32.add (local.get $bitsCur)(local.get $pageBits)))

      (block $exit (loop $write (br_if $exit (i32.lt_s (local.get $bitsCur)(i32.const 8)))
        (i32.store8 (local.get $outCur)(local.get $accum))

        (local.set $outCur (i32.add (local.get $outCur)(i32.const 1)))
        (local.set $accum (i32.shr_u (local.get $accum)(i32.const 8)))
        (local.set $bitsCur (i32.sub (local.get $bitsCur)(i32.const 8)))
        (local.set $blockCnt (i32.add (local.get $blockCnt)(i32.const 1)))

        (if (i32.ge_u (local.get $blockCnt)(i32.const 255)) (then
          (i32.store8 (local.get $sizeCur)(i32.const 255))

          (local.set $sizeCur (local.get $outCur))
          (local.set $outCur (i32.add (local.get $outCur)(i32.const 1)))
          (local.set $blockCnt (i32.const 0))

          (if (i32.ge_u (local.get $outCur)(i32.sub (i32.mul (memory.size)(i32.const MEMPAGESIZE))(i32.const 256))) (then
            (memory.grow (i32.const 1))
            drop
          ))
        ))

        br $write
      ))

      (local.set $ent (local.get $c))

      (if (i32.gt_s (local.get $freeEnt)(local.get $pageSize)) (then
        (local.set $pageBits (i32.add (local.get $pageBits)(i32.const 1)))

        (if (i32.eq (local.get $pageBits)(i32.const MAXBITS))
          (then (local.set $pageSize (i32.const MAXPAGESIZE)))
          (else (local.set $pageSize (i32.sub (i32.shl (i32.const 1)(local.get $pageBits))(i32.const 1)))))
      ))

      (if (i32.lt_s (local.get $freeEnt)(i32.const MAXPAGESIZE)) (then
        (i32.store align=4 (i32.mul (local.get $i)(i32.const 4))(i32.wrap_i64 (local.get $fcode)))
        (i32.store align=4 (i32.mul (i32.add (i32.const HSIZE)(local.get $i))(i32.const 4))(local.get $freeEnt))
        
        (local.set $freeEnt (i32.add (local.get $freeEnt)(i32.const 1)))

      ) (else
        (memory.fill (i32.const 0)(i32.const 255)(i32.const HMEMSIZE))

        (local.set $freeEnt (i32.add (local.get $clearCode)(i32.const 2)))
        (local.set $accum (i32.or (local.get $accum)(i32.shl (local.get $clearCode)(local.get $bitsCur))))
        (local.set $bitsCur (i32.add (local.get $bitsCur)(local.get $pageBits)))
        (local.set $pageBits (local.get $initBits))
        (local.set $pageSize (i32.sub (i32.shl (i32.const 1)(local.get $pageBits))(i32.const 1)))
      ))

      br $nextPx
    ))

    (local.set $accum (i32.or (local.get $accum)(i32.shl (local.get $ent)(local.get $bitsCur))))
    (local.set $bitsCur (i32.add (local.get $bitsCur)(local.get $pageBits)))
    (local.set $accum (i32.or (local.get $accum)(i32.shl (i32.add (local.get $clearCode)(i32.const 1))(local.get $bitsCur))))
    (local.set $bitsCur (i32.add (local.get $bitsCur)(local.get $pageBits)))

    (block $exit (loop $write (br_if $exit (i32.le_s (local.get $bitsCur)(i32.const 0)))
      (i32.store8 (local.get $outCur)(local.get $accum))

      (local.set $outCur (i32.add (local.get $outCur)(i32.const 1)))
      (local.set $accum (i32.shr_u (local.get $accum)(i32.const 8)))
      (local.set $bitsCur (i32.sub (local.get $bitsCur)(i32.const 8)))
      (local.set $blockCnt (i32.add (local.get $blockCnt)(i32.const 1)))

      (if (i32.ge_u (local.get $blockCnt)(i32.const 255)) (then
        (i32.store8 (local.get $sizeCur)(i32.const 255))

        (local.set $sizeCur (local.get $outCur))
        (local.set $outCur (i32.add (local.get $outCur)(i32.const 1)))
        (local.set $blockCnt (i32.const 0))

        (if (i32.ge_u (local.get $outCur)(i32.sub (i32.mul (memory.size)(i32.const MEMPAGESIZE))(i32.const 256))) (then
          (memory.grow (i32.const 1))
          drop
        ))
      ))

      br $write
    ))
      
    (i32.store8 (local.get $sizeCur)(local.get $blockCnt))

    (if (local.get $blockCnt) (then
      (i32.store8 (local.get $outCur)(i32.const 0))
      (local.set $outCur (i32.add (local.get $outCur)(i32.const 1)))
    ))

    (i32.sub (local.get $outCur)(i32.add (i32.const WORKMEMSIZE)(local.get $imgLength)))

    #undef MAXBITS
    #undef HSIZE
    #undef HSHIFT
    #undef MAXPAGESIZE
  )

  #define CEILDIV(lhs, rhs) i32.div_u (i32.add (lhs)(i32.sub (rhs)(i32.const 1)))(rhs)
  #define PADDED(value, padNum) i32.mul (CEILDIV(value, i32.const padNum))(i32.const padNum)
  #define INCREMENT(label, num) (local.set label (i32.add (local.get label)(i32.const num)))
  #define DECREMENT(label, num) (local.set label (i32.sub (local.get label)(i32.const num)))
  #define TEST(label, num) i32.and (local.get label)(i32.const num)
  #define GETPX(val) i32.load align=4 (val)
  #define SWAPPX(labelA, labelB)(local.get labelA)(local.get labelB)(i32.load align=4)(local.get labelB)(local.get labelA)(i32.load align=4)(i32.store align=4)(i32.store align=4)
  #define MEDIANPX(valA, valB) i32.and (i32.shr_u (i32.add (valA)(valB))(i32.const 1))(i32.const 0xfffffffc)
  #define MAX(labelA, labelB) select (local.get labelA)(local.get labelB)(i32.gt_s (local.get labelA)(local.get labelB))

  #define _GETLOW(nodeAddr) i32.add (nodeAddr)(i32.const 8)
  #define _GETHIGH(nodeAddr) i32.add (nodeAddr)(i32.mul (i32.add (i32.load8_u offset=7 (nodeAddr))(i32.const 1))(i32.const 8))
  #define _TESTNODE(pxValue, maskValue, thrValue) i32.lt_u (i32.and (pxValue)(maskValue))(thrValue)
  #define GETNEXTNODE(pxValue, nodeAddr, maskValue, thrValue) if (result i32)(_TESTNODE(pxValue, maskValue, thrValue))(then (_GETLOW(nodeAddr)))(else (_GETHIGH(nodeAddr)))
  #define READNODE(nodeAddr, maskLabel, thrLabel) (local.set maskLabel (i32.load offset=0 align=4 (nodeAddr)))(local.set thrLabel (i32.and (i32.load offset=4 align=4 (nodeAddr))(local.get maskLabel)))
  #define WRITENODE(nodeAddr, maskValue, thrValue) (i32.store offset=0 align=4 (nodeAddr)(maskValue))(i32.store offset=4 align=4 (nodeAddr)(i32.and (thrValue)(maskValue)))
  #define WRITEHIGHNODEADDR(parentNodeAddr, highNodeAddr) (i32.store8 offset=7 (parentNodeAddr)(i32.sub (i32.shr_u (i32.sub (highNodeAddr)(parentNodeAddr))(i32.const 3))(i32.const 1)))
  #define WRITETERMINALNODE(nodeAddr, value) (i32.store offset=0 align=4 (nodeAddr)(i32.const -1))(i32.store offset=4 align=4 (nodeAddr)(value))

  (func $growMem (param $need i32)
    (local.set $need (CEILDIV(local.get $need, i32.const MEMPAGESIZE)))
    (if (i32.lt_u (memory.size)(local.get $need)) (then
      (drop (memory.grow (i32.sub (local.get $need)(memory.size))))
    ))
  )

  (func $initImgData (param $len i32)(param $flag i32)(result i32 i32 i32 i32)
    (local $imgBeg i32)
    (local $imgEnd i32)
    (local $cpyBeg i32)
    (local $cpyEnd i32)
    (local $cursor i32)
    (local $vec v128)
    (local $vecMask v128)

    (local.set $imgBeg (i32.const WORKMEMSIZE))
    (local.set $imgEnd (i32.add (local.get $imgBeg)(local.get $len)))
    (local.set $cpyBeg (PADDED(local.get $imgEnd, 16)))
    (local.set $cpyEnd (i32.add (local.get $cpyBeg)(local.get $len)))
    (call $growMem (i32.add (local.get $cpyEnd)(i32.const 16))) ;; simd margin
    
    (if (TEST($flag, TRANSPARENTFLAG))(then
      (local.set $cursor (local.get $imgBeg))
      (block $exit
        (loop $next (br_if $exit (i32.ge_u (local.get $cursor)(local.get $imgEnd)))
          (local.set $vec (v128.load align=16 (local.get $cursor)))
          (local.set $vecMask
            (i32x4.lt_s
              (local.get $vec)
              (v128.const i32x4 0x00000000 0x00000000 0x00000000 0x00000000)))
          (v128.store align=16
            (local.get $cursor)
            (v128.or
              (v128.and
                (local.get $vec)
                (local.get $vecMask))
              (v128.and
                (v128.const i32x4 0xff000000 0xff000000 0xff000000 0xff000000)
                (local.get $vecMask))))
          INCREMENT($cursor, 16)
          br $next
        )
      )
    )(else
      (block $exit
        (loop $next (br_if $exit (i32.ge_u (local.get $cursor)(local.get $imgEnd)))
          (v128.store align=16
            (local.get $cursor)
            (v128.or
              (v128.load align=16 (local.get $cursor))
              (v128.const i32x4 0xff000000 0xff000000 0xff000000 0xff000000)))
          INCREMENT($cursor, 16)
          br $next
        )
      )
    ))
    (memory.copy
      (local.get $cpyBeg)
      (local.get $imgBeg)
      (local.get $len))
    
    (local.get $imgBeg)
    (local.get $imgEnd)
    (local.get $cpyBeg)
    (local.get $cpyEnd)
  )

  (func $pixelQsort (param $beg i32)(param $end i32)(param $mask i32)
    (;
    http://ndevilla.free.fr/median/median/index.html

    /*----------------------------------------------------------------------------
      Comparison of qsort-based and optimized median search methods
      Nicolas Devillard <ndevilla@free.fr> August 1998
      This code in public domain.
    ---------------------------------------------------------------------------*/
    ;)

    (local $left i32)
    (local $right i32)
    (local $median i32)
    (local $ll i32)
    (local $rr i32)
    (local $pixBuf i32)
    (local $maskedPixBuf i32)
    (local $lBehind i32)
    (local $rBehind i32)
    (local $llBehind i32)
    (local $rrBehind i32)
    (local $stack i32)

    #define GETMASKEDPX(value) i32.and (GETPX(value))(local.get $mask)
    #define BACKUPPX(addr, label, maskedLabel) (local.set label (GETPX(addr)))(local.set maskedLabel (i32.and (local.get label)(local.get $mask)))
    #define RESTOREPX(addr, label) (i32.store align=4 (addr)(local.get label))
    #define COPYPX(dstAddr, srcAddr) (i32.store align=4 (dstAddr)(GETPX(srcAddr)))
    #define PUSH(valA, valB) (i32.store offset=0 align=4 (local.get $stack)(valA))(i32.store offset=4 align=4 (local.get $stack)(valB))INCREMENT($stack, 8)
    #define POP(labelA, labelB) DECREMENT($stack, 8)(local.set labelA(i32.load offset=0 align=4 (local.get $stack)))(local.set labelB(i32.load offset=4 align=4 (local.get $stack)))
    #define ISORTTHR 28

    INCREMENT($beg, 4)
    (local.set $left (local.get $beg))
    (local.set $right (local.get $end))
    (local.set $stack (i32.const SORTSTACKADDR))

    (block $done (loop $next
      (if (i32.lt_u (i32.sub (local.get $right)(local.get $left))(i32.const ISORTTHR)) (then
        (local.set $rr (i32.add (local.get $left)(i32.const 4)))
        (block $iSortRExit (loop $iSortRNext
          (br_if $iSortRExit (i32.gt_u (local.get $rr)(local.get $right)))
          (local.set $rrBehind (i32.sub (local.get $rr)(i32.const 4)))
          BACKUPPX(local.get $rrBehind, $pixBuf, $maskedPixBuf)
          (local.set $ll (local.get $rrBehind))
          (block $iSortLExit (loop $iSortLNext
            (br_if $iSortLExit (i32.lt_u (local.get $ll)(local.get $beg)))
            (local.set $llBehind (i32.sub (local.get $ll)(i32.const 4)))
            (br_if $iSortLExit (i32.le_u (GETMASKEDPX(local.get $llBehind))(local.get $maskedPixBuf)))
            COPYPX(local.get $ll, local.get $llBehind)
            DECREMENT($ll, 4)
            br $iSortLNext
          ))
          RESTOREPX(local.get $ll, $pixBuf)
          INCREMENT($rr, 4)
          br $iSortRNext
        ))
        (br_if $done (i32.eq (local.get $stack)(i32.const SORTSTACKADDR)))
        POP($left, $right)
      )(else
        (local.set $median (i32.sub (MEDIANPX(local.get $left, local.get $right))(i32.const 4)))
        SWAPPX($median, $left)
        (local.set $lBehind (i32.sub (local.get $left)(i32.const 4)))
        (local.set $rBehind (i32.sub (local.get $right)(i32.const 4)))
        (if (i32.gt_u(GETMASKEDPX(local.get $left))(GETMASKEDPX(local.get $rBehind)))(then
          SWAPPX($left, $rBehind)
        ))
        (if (i32.gt_u(GETMASKEDPX(local.get $lBehind))(GETMASKEDPX(local.get $rBehind)))(then
          SWAPPX($lBehind, $rBehind)
        ))
        (if (i32.gt_u(GETMASKEDPX(local.get $left))(GETMASKEDPX(local.get $lBehind)))(then
          SWAPPX($left, $lBehind)
        ))
        (local.set $ll (i32.add (local.get $left)(i32.const 4)))
        (local.set $rr (local.get $right))
        BACKUPPX(local.get $lBehind, $pixBuf, $maskedPixBuf)
        (block $partitionExit (loop $partitionNext
          (block $advExit (loop $advNext
            (local.set $llBehind (local.get $ll))
            INCREMENT($ll, 4)
            (br_if $advExit (i32.ge_u (GETMASKEDPX(local.get $llBehind))(local.get $maskedPixBuf)))
            br $advNext
          ))
          (block $bacExit (loop $bacNext
            DECREMENT($rr, 4)
            (local.set $rrBehind (i32.sub (local.get $rr)(i32.const 4)))
            (br_if $bacExit (i32.le_u (GETMASKEDPX(local.get $rrBehind))(local.get $maskedPixBuf)))
            br $bacNext
          ))
          (br_if $partitionExit (i32.lt_u (local.get $rr)(local.get $ll)))
          SWAPPX($llBehind, $rrBehind)
          br $partitionNext
        ))
        (local.set $rrBehind (i32.sub (local.get $rr)(i32.const 4)))
        COPYPX(local.get $lBehind, local.get $rrBehind)
        RESTOREPX(local.get $rrBehind, $pixBuf)
        (if (i32.ge_u
          (i32.add (i32.sub (local.get $right)(local.get $ll))(i32.const 4))
          (i32.sub(local.get $rr)(local.get $left)))(then
          PUSH(local.get $ll, local.get $right)
          (local.set $right (local.get $rrBehind))
        )(else
          PUSH(local.get $left, local.get $rrBehind)
          (local.set $left (local.get $ll))
        ))
      ))
      br $next
    ))

    #undef GETMASKEDPX
    #undef BACKUPPX
    #undef RESTOREPX
    #undef COPYPX
    #undef PUSH
    #undef POP
    #undef ISORTTHR
  )

  (func $quickSelect (param $beg i32)(param $end i32)(param $mask i32)(result i32)
    (;
      http://ndevilla.free.fr/median/median/index.html

      /*
       * Algorithm from N. Wirth's book, implementation by N. Devillard.
       * This code in public domain.
       */
    ;)
    (local $low i32)
    (local $high i32)
    (local $median i32)
    (local $middle i32)
    (local $ll i32)
    (local $hh i32)

    #define GETMASKEDPX(value) i32.and (GETPX(value))(local.get $mask)

    (local.set $low  (local.get $beg))
    (local.set $high (i32.sub (local.get $end)(i32.const 4)))
    (local.set $median (MEDIANPX(local.get $low, local.get $end)))

    (block $done (loop $continue
      (br_if $done (i32.ge_u (local.get $low)(local.get $high)))
      (if (i32.eq (local.get $high)(i32.add (local.get $low)(i32.const 4)))(then
        (if (i32.gt_u (GETMASKEDPX(local.get $low))(GETMASKEDPX(local.get $high)))(then
          SWAPPX($low, $high)
        ))
        br $done
      ))

      (local.set $middle (GETMASKEDPX(local.get $median)))
      (local.set $ll (local.get $low))
      (local.set $hh (local.get $high))
      (block $partitionExit (loop $partitionNext
        (block $exitLL (loop $advLL
          (br_if $exitLL (i32.ge_u (GETMASKEDPX(local.get $ll))(local.get $middle)))
          INCREMENT($ll, 4)
          br $advLL
        ))
        (block $exitHH (loop $bacHH
          (br_if $exitHH (i32.ge_u (local.get $middle)(GETMASKEDPX(local.get $hh))))
          DECREMENT($hh, 4)
          br $bacHH
        ))
        (if (i32.le_u (local.get $ll)(local.get $hh))(then
          SWAPPX($ll, $hh)
          INCREMENT($ll, 4)
          DECREMENT($hh, 4)
          br $partitionNext
        ))
        br $partitionExit
      ))
      (if (i32.lt_u (local.get $hh)(local.get $median))(then
        (local.set $low (local.get $ll))
      ))
      (if (i32.lt_u (local.get $median)(local.get $ll))(then
        (local.set $high (local.get $hh))
      ))
      br $continue
    ))

    local.get $median

    #undef GETMASKEDPX
  )

  (func $averagePos (param $beg i32)(param $end i32)(param $mask i32)(result i32)
    (local $min i32)
    (local $max i32)
    (local $avg i32)
    (local $cur i32)

    #define GETMASKEDPX(value) i32.and (GETPX(value))(local.get $mask)
    
    (call $pixelQsort(local.get $beg)(local.get $end)(local.get $mask))
    (local.set $end (i32.sub (local.get $end)(i32.const 4)))
    (local.set $min (GETMASKEDPX(local.get $beg)))
    (local.set $max (GETMASKEDPX(local.get $end)))
    (local.set $avg (i32.shr_u (i32.add (local.get $min)(local.get $max))(i32.const 1)))

    (if (i32.eq (local.get $min)(local.get $avg))(then
      (local.set $avg (local.get $max))
    ))

    (block $done
      (local.set $cur (local.get $beg))
      (loop $continue 
        (br_if $done (i32.ge_u (local.get $cur)(local.get $end)))
        (br_if $done (i32.ge_u (GETMASKEDPX(local.get $cur))(local.get $avg)))
        INCREMENT($cur, 4)
        br $continue
      )
    )

    (local.get $cur)
    #undef GETMASKEDPX
  )

  (func $excludeTransparent (param $beg i32)(param $end i32)(result i32)
    (local $cursor i32)
    (local $median i32)

    #define LSEARCHTHR 256


    (local.set $cursor (local.get $beg))
    (if (i32.eqz (GETPX(local.get $cursor)))(then
      (block $done (loop $next
        (local.set $median (MEDIANPX(local.get $cursor, local.get $end)))
        (if (GETPX(local.get $median))(then
          (local.set $end (local.get $median))
        )(else
          (local.set $cursor (local.get $median))
        ))
        (br_if $done (i32.le_u (i32.sub (local.get $end)(local.get $cursor))(i32.const LSEARCHTHR)))
        br $next
      ))
      (block $done (loop $next
        (br_if $done (i32.ge_u (local.get $cursor)(local.get $end)))
        (br_if $done (GETPX(local.get $cursor)))
        INCREMENT($cursor, 4)
        br $next
      ))
    ))

    local.get $cursor

    #undef LSEARCHTHR
  )

  (func $makePixelSet (param $beg i32)(param $end i32)(param $bitDepth i32)(result i32)
    (local $px i32)
    (local $rgb i32)
    (local $prev i32)
    (local $dst i32)
    (local $count i32)
    (local $sizeThr i32)

    (block $exit
      (local.set $px (local.get $beg))
      (local.set $prev (i32.const -1))
      (local.set $dst (local.get $beg))
      (local.set $count (i32.const 0))
      (local.set $sizeThr (i32.shl (i32.const 1)(local.get $bitDepth)))
      
      (loop $next
        (br_if $exit (i32.ge_u (local.get $px)(local.get $end)))
        (br_if $exit (i32.ge_u (local.get $count)(local.get $sizeThr)))
        (local.set $rgb (GETPX(local.get $px)))
        (if (i32.ne (local.get $prev)(local.get $rgb))(then
          (i32.store align=4 (local.get $dst)(local.get $rgb))
          (local.set $prev (local.get $rgb))
          INCREMENT($dst, 4)
          INCREMENT($count, 1)
        ))
        INCREMENT($px, 4)
        br $next
    ))

    local.get $dst
  )

  (func $getRGBMask (param $beg i32)(param $end i32)(result i32)
    (local $simdBeg i32)
    (local $simdEnd i32)
    (local $cur i32)
    (local $min v128)
    (local $max v128)
    (local $vec v128)
    (local $mask v128)
    (local $len v128)
    (local $rLen i32)
    (local $gLen i32)
    (local $bLen i32)
    (local $lenMax i32)

    (local.set $min (v128.const i32x4 0xffffffff 0xffffffff 0xffffffff 0xffffffff))
    (local.set $max (v128.const i32x4 0x00000000 0x00000000 0x00000000 0x00000000))
    (local.set $simdBeg (PADDED(local.get $beg, 16)))
    (local.set $simdEnd (i32.and (local.get $end)(i32.const 0xfffffff0)))

    (local.set $cur (local.get $beg))
    (if (i32.lt_u (local.get $simdBeg)(local.get $simdEnd))(then
      (block $exit (loop $next (br_if $exit (i32.ge_u (local.get $cur)(local.get $simdBeg)))
        (local.set $vec(v128.load32_splat align=4 (local.get $cur)))
        (local.set $min (i8x16.min_u (local.get $min)(local.get $vec)))
        (local.set $max (i8x16.max_u (local.get $max)(local.get $vec)))
        INCREMENT($cur, 4)
        br $next
      ))
      (block $exit (loop $next (br_if $exit (i32.ge_u (local.get $cur)(local.get $simdEnd)))
        (local.set $vec (v128.load align=16 (local.get $cur)))
        (local.set $min (i8x16.min_u (local.get $min)(local.get $vec)))
        (local.set $max (i8x16.max_u (local.get $max)(local.get $vec)))
        INCREMENT($cur, 16)
        br $next
      ))
    ))
    (block $exit (loop $next (br_if $exit (i32.ge_u (local.get $cur)(local.get $end)))
      (local.set $vec(v128.load32_splat align=4 (local.get $cur)))
      (local.set $min (i8x16.min_u (local.get $min)(local.get $vec)))
      (local.set $max (i8x16.max_u (local.get $max)(local.get $vec)))
      INCREMENT($cur, 4)
      br $next
    ))

    (local.set $vec (v128.const i8x16 0x08 0x09 0x0a 0x0b 0x0c 0x0d 0x0e 0x0f 0x00 0x01 0x02 0x03 0x04 0x05 0x06 0x07))
    (local.set $min (i8x16.min_u (local.get $min)(i8x16.swizzle (local.get $min)(local.get $vec))))
    (local.set $max (i8x16.max_u (local.get $max)(i8x16.swizzle (local.get $max)(local.get $vec))))
    (local.set $vec (v128.const i8x16 0x04 0x05 0x06 0x07 0x08 0x09 0x0a 0x0b 0x0c 0x0d 0x0e 0x0f 0x00 0x01 0x02 0x03))
    (local.set $min (i8x16.min_u (local.get $min)(i8x16.swizzle (local.get $min)(local.get $vec))))
    (local.set $max (i8x16.max_u (local.get $max)(i8x16.swizzle (local.get $max)(local.get $vec))))
    
    (local.set $len (i8x16.sub (local.get $max)(local.get $min)))

    (local.set $rLen(i8x16.extract_lane_u 0 (local.get $len)))
    (local.set $gLen(i8x16.extract_lane_u 1 (local.get $len)))
    (local.set $bLen(i8x16.extract_lane_u 2 (local.get $len)))

    (local.set $lenMax (MAX($bLen, $gLen)))
    (local.set $lenMax (MAX($lenMax, $rLen)))
    (if (result i32)(i32.eq(local.get $lenMax)(local.get $rLen))(then
      (i32.const 0xff)
    )(else (if (result i32)(i32.eq(local.get $lenMax)(local.get $gLen))(then
      (i32.const 0xff00)
    )(else 
      (i32.const 0xff0000)
    ))))
  )

  (func $writePaletteData (param $beg i32)(param $end i32)(param $palette i32)(result i32)
    (local $readCur i32)
    (local $rColor i32)
    (local $gColor i32)
    (local $bColor i32)
    (local $pxCount i32)
    (local $writeCur i32)

    (local.set $readCur (local.get $beg))
    (local.set $rColor (i32.const 0))
    (local.set $gColor (i32.const 0))
    (local.set $bColor (i32.const 0))

    (block $exit (loop $next (br_if $exit (i32.ge_u (local.get $readCur)(local.get $end)))
      (local.set $rColor (i32.add (local.get $rColor)(i32.load8_u offset=0 (local.get $readCur))))
      (local.set $gColor (i32.add (local.get $gColor)(i32.load8_u offset=1 (local.get $readCur))))
      (local.set $bColor (i32.add (local.get $bColor)(i32.load8_u offset=2 (local.get $readCur))))
      (local.set $readCur (i32.add (local.get $readCur)(i32.const 4)))
      br $next
    ))

    (local.set $pxCount (i32.shr_u (i32.sub (local.get $end)(local.get $beg))(i32.const 2)))
    (local.set $writeCur (i32.mul (local.get $palette)(i32.const 3)))
    (i32.store8 offset=0
      (local.get $writeCur)
      (i32.div_u
        (local.get $rColor)
        (local.get $pxCount)))
    (i32.store8 offset=1
      (local.get $writeCur)
      (i32.div_u
        (local.get $gColor)
        (local.get $pxCount)))
    (i32.store8 offset=2
      (local.get $writeCur)
      (i32.div_u
        (local.get $bColor)
        (local.get $pxCount)))
    
    (i32.add (local.get $palette)(i32.const 1))
  )

  (func $averageCut (param $beg i32)(param $end i32)(param $bitDepth i32)(param $palette i32)
    (local $stack i32)
    (local $mask i32)
    (local $average i32)
    (local $nodeCur i32)
    (local $parentNode i32)
    (local $paletteThr i32)
    (local $paletteFlag i32)
    
    #define PUSH(valA, valB, valC) (i32.store offset=0 align=4 (local.get $stack)(valA))(i32.store offset=4 align=4 (local.get $stack)(valB))(i32.store offset=8 align=4 (local.get $stack)(valC))(local.set $stack (i32.add (local.get $stack)(i32.const 12)))
    #define POP(labelA, labelB, labelC) (local.set $stack (i32.sub (local.get $stack)(i32.const 12)))(local.set labelA (i32.load offset=0 align=4 (local.get $stack)))(local.set labelB (i32.load offset=4 align=4 (local.get $stack)))(local.set labelC (i32.load offset=8 align=4 (local.get $stack)))

    (local.set $stack (i32.const MEDIANCUTSTACKADDR))
    (local.set $nodeCur (i32.const TREEADDR))
    (local.set $parentNode (i32.const -1))
    (local.set $paletteThr (i32.sub (i32.shl (i32.const 1)(local.get $bitDepth))(i32.const 1)))

    (memory.fill (i32.const 0)(i32.const 0)(i32.const MEDIANCUTSTACKADDR))

    (block $done
      (loop $next
        (local.set $paletteFlag
          (if (result i32)(i32.eq (i32.load (local.get $beg))(i32.load (i32.sub (local.get $end)(i32.const 4))))(then
            (local.set $end (i32.add (local.get $beg)(i32.const 4)))
            (i32.const 1)
          )(else (if (result i32)(i32.ge_u (local.get $palette)(local.get $paletteThr))(then
            (i32.const 1)
          )(else
            (i32.const 0)
          )))))
        
        (if (local.get $paletteFlag)(then
          WRITETERMINALNODE(local.get $nodeCur, local.get $palette)
          INCREMENT($nodeCur, 8)
          (local.set $palette 
            (call $writePaletteData
              (local.get $beg)
              (local.get $end)
              (local.get $palette)))
          (br_if $done (i32.gt_u (local.get $palette)(local.get $paletteThr)))
        )(else
          (local.set $mask (call $getRGBMask(local.get $beg)(local.get $end)))
          (local.set $average (call $averagePos (local.get $beg)(local.get $end)(local.get $mask)))
          WRITENODE(local.get $nodeCur, local.get $mask, i32.load (local.get $average))
          (if (i32.lt_u (local.get $average)(local.get $end))(then
            PUSH(local.get $average, local.get $end, local.get $nodeCur)
          ))
          (if (i32.lt_u (local.get $beg)(local.get $average))(then
            PUSH(local.get $beg, local.get $average, i32.const -1)
          ))
          INCREMENT($nodeCur, 8)
        ))

        (br_if $done (i32.le_u (local.get $stack)(i32.const MEDIANCUTSTACKADDR)))
        POP($beg, $end, $parentNode)

        (if (i32.ge_s(local.get $parentNode)(i32.const 0))(then
          WRITEHIGHNODEADDR(local.get $parentNode, local.get $nodeCur)
        ))
        
        br $next
      )
    )

    #undef PUSH
    #undef POP
  )

  (func $medianCut (param $beg i32)(param $end i32)(param $bitDepth i32)(param $palette i32)
    (local $stack i32)
    (local $mask i32)
    (local $median i32)
    (local $nodeCur i32)
    (local $parentNode i32)
    (local $paletteThr i32)
    
    #define PUSH(valA, valB, valC, valD) (i32.store offset=0 align=4 (local.get $stack)(valA))(i32.store offset=4 align=4 (local.get $stack)(valB))(i32.store offset=8 align=4 (local.get $stack)(valC))(i32.store offset=12 align=4 (local.get $stack)(valD))(local.set $stack (i32.add (local.get $stack)(i32.const 16)))
    #define POP(labelA, labelB, labelC, labelD) (local.set $stack (i32.sub (local.get $stack)(i32.const 16)))(local.set labelA (i32.load offset=0 align=4 (local.get $stack)))(local.set labelB (i32.load offset=4 align=4 (local.get $stack)))(local.set labelC (i32.load offset=8 align=4 (local.get $stack)))(local.set labelD (i32.load offset=12 align=4 (local.get $stack)))

    (local.set $stack (i32.const MEDIANCUTSTACKADDR))
    (local.set $nodeCur (i32.const TREEADDR))
    (local.set $parentNode (i32.const -1))
    (local.set $paletteThr (i32.sub (i32.shl (i32.const 1)(local.get $bitDepth))(i32.const 1)))

    (memory.fill (i32.const 0)(i32.const 0)(i32.const MEDIANCUTSTACKADDR))

    (block $done
      (loop $next
        (if (i32.or
          (i32.eqz (local.get $bitDepth))
          (i32.ge_u (local.get $palette)(local.get $paletteThr)))(then
          WRITETERMINALNODE(local.get $nodeCur, local.get $palette)
          INCREMENT($nodeCur, 8)
          (local.set $palette 
            (call $writePaletteData
              (local.get $beg)
              (local.get $end)
              (local.get $palette)))
          (br_if $done (i32.gt_u (local.get $palette)(local.get $paletteThr)))
        )(else
          (local.set $mask (call $getRGBMask(local.get $beg)(local.get $end)))
          (local.set $median (call $quickSelect (local.get $beg)(local.get $end)(local.get $mask)))
          WRITENODE(local.get $nodeCur, local.get $mask, i32.load (local.get $median))
          DECREMENT($bitDepth, 1)
          (if (i32.lt_u (local.get $median)(local.get $end))(then
            PUSH(local.get $median, local.get $end, local.get $bitDepth, local.get $nodeCur)
          ))
          (if (i32.lt_u (local.get $beg)(local.get $median))(then
            PUSH(local.get $beg, local.get $median, local.get $bitDepth, i32.const -1)
          ))
          INCREMENT($nodeCur, 8)
        ))

        (br_if $done (i32.le_u (local.get $stack)(i32.const MEDIANCUTSTACKADDR)))
        POP($beg, $end, $bitDepth, $parentNode)

        (if (i32.ge_s(local.get $parentNode)(i32.const 0))(then
          WRITEHIGHNODEADDR(local.get $parentNode, local.get $nodeCur)
        ))
        
        br $next
      )
    )

    #undef PUSH
    #undef POP
  )

  (func $makeIdxImg (param $dst i32)(param $beg i32)(param $end i32)(param $bitDepth i32)
    (local $cursor i32)
    (local $px i32)
    (local $node i32)
    (local $mask i32)
    (local $thr i32)

    (local.set $cursor (local.get $beg))
    (block $done
      (loop $next (br_if $done (i32.ge_u (local.get $cursor)(local.get $end)))
        (local.set $px (i32.load align=4 (local.get $cursor)))
        (if (i32.ge_s(local.get $px)(i32.const 0))(then
          (i32.store8 (local.get $dst)(i32.const 0))
          INCREMENT($dst, 1)
          INCREMENT($cursor, 4)
          br $next
        ))

        (local.set $node (i32.const TREEADDR))
        (block $find
          (loop $dig
            READNODE(local.get $node, $mask, $thr)
            (if (i32.eq (local.get $mask)(i32.const -1))(then
              (i32.store8 (local.get $dst)(local.get $thr))
              INCREMENT($dst, 1)
              br $find
            ))
            (local.set $node (GETNEXTNODE(local.get $px, local.get $node, local.get $mask, local.get $thr)))
            br $dig
          )
        )
        INCREMENT($cursor, 4)
        br $next
      )
    )
  )

  (func $dithering (param $dst i32)(param $beg i32)(param $end i32)(param $width i32)(param $bitDepth i32)
    (local $cursor i32)
    (local $px i32)
    (local $node i32)
    (local $mask i32)
    (local $thr i32)

    (local $x i32)
    (local $wBytes i32)
    (local $bottom i32)
    (local $neighbor i32)

    (local $error v128)
    (local $rgba v128)

    #define LOADPX2VEC(addr) i16x8.extend_low_i8x16_u (v128.load8x8_u (addr))
    ;; diffuse errors by 50%
    #define DIFFUSE(srcAddr, err, n) i32x4.add (LOADPX2VEC(srcAddr))(i32x4.shr_s (i32x4.mul (err)(v128.const i32x4 n  n  n  n))(i32.const 5))
    #define CLAMP(label) (local.set label (i32x4.min_s (i32x4.max_s (v128.const i32x4 0x00 0x00 0x00 0x00)(local.get label))(v128.const i32x4 0xff 0xff 0xff 0xff)))
    
    (local.set $cursor (local.get $beg))
    (local.set $x (i32.const 0))
    (local.set $wBytes (i32.mul (local.get $width)(i32.const 4)))
    (local.set $bottom (i32.sub (local.get $end)(local.get $wBytes)))
    (block $done
      (loop $next (br_if $done (i32.ge_u (local.get $cursor)(local.get $end)))
        (local.set $px (i32.load align=4 (local.get $cursor)))
        (if (i32.ge_s(local.get $px)(i32.const 0))(then
          (i32.store8 (local.get $dst)(i32.const 0))
          INCREMENT($dst, 1)
          INCREMENT($cursor, 4)
          br $next
        ))

        (local.set $node (i32.const TREEADDR))
        (block $find
          (loop $dig
            READNODE(local.get $node, $mask, $thr)
            (if (i32.eq (local.get $mask)(i32.const -1))(then
              (local.set $error
                (i32x4.sub 
                  (LOADPX2VEC(local.get $cursor))
                  (LOADPX2VEC(i32.mul (local.get $thr)(i32.const 3)))))

              (if (i32.lt_u (i32.add (local.get $x)(i32.const 1))(local.get $width))(then
                (local.set $neighbor (i32.add (local.get $cursor)(i32.const 4)))
                (local.set $rgba (DIFFUSE(local.get $neighbor, local.get $error, 7)))
                CLAMP($rgba)
                (i32.store8 offset=0 (local.get $neighbor)(i32x4.extract_lane 0 (local.get $rgba)))
                (i32.store8 offset=1 (local.get $neighbor)(i32x4.extract_lane 1 (local.get $rgba)))
                (i32.store8 offset=2 (local.get $neighbor)(i32x4.extract_lane 2 (local.get $rgba)))
                (if (i32.lt_u (local.get $cursor)(local.get $bottom))(then
                  (local.set $neighbor (i32.add (local.get $neighbor)(local.get $wBytes)))
                  (local.set $rgba (DIFFUSE(local.get $neighbor, local.get $error, 1)))
                  CLAMP($rgba)
                  (i32.store8 offset=0 (local.get $neighbor)(i32x4.extract_lane 0 (local.get $rgba)))
                  (i32.store8 offset=1 (local.get $neighbor)(i32x4.extract_lane 1 (local.get $rgba)))
                  (i32.store8 offset=2 (local.get $neighbor)(i32x4.extract_lane 2 (local.get $rgba)))
                ))
              ))

              (if (i32.lt_u (local.get $cursor)(local.get $bottom))(then
                (local.set $neighbor (i32.add (local.get $cursor)(local.get $wBytes)))
                (local.set $rgba (DIFFUSE(local.get $neighbor, local.get $error, 5)))
                CLAMP($rgba)
                (i32.store8 offset=0 (local.get $neighbor)(i32x4.extract_lane 0 (local.get $rgba)))
                (i32.store8 offset=1 (local.get $neighbor)(i32x4.extract_lane 1 (local.get $rgba)))
                (i32.store8 offset=2 (local.get $neighbor)(i32x4.extract_lane 2 (local.get $rgba)))
                (if (i32.gt_u (local.get $x)(i32.const 0))(then
                  (local.set $neighbor (i32.sub (local.get $neighbor)(i32.const 4)))
                  (local.set $rgba (DIFFUSE(local.get $neighbor, local.get $error, 3)))
                  CLAMP($rgba)
                  (i32.store8 offset=0 (local.get $neighbor)(i32x4.extract_lane 0 (local.get $rgba)))
                  (i32.store8 offset=1 (local.get $neighbor)(i32x4.extract_lane 1 (local.get $rgba)))
                  (i32.store8 offset=2 (local.get $neighbor)(i32x4.extract_lane 2 (local.get $rgba)))
                ))
              ))

              (i32.store8 (local.get $dst)(local.get $thr))
              INCREMENT($dst, 1)

              (local.set $x (i32.rem_u (i32.add (local.get $x)(i32.const 1))(local.get $width)))

              br $find
            ))

            (local.set $node (GETNEXTNODE(local.get $px, local.get $node, local.get $mask, local.get $thr)))
            br $dig
          )
        )
        INCREMENT($cursor, 4)
        br $next
      )
    )
    
    #undef LOADPX2VEC
    #undef DIFFUSE
    #undef CLAMP
  )

  (func (export "calcPalette")(param $imgLength i32)(param $width i32)(param $bitDepth i32)(param $bgColor i32)(param $flag i32)
    (local $mapCur i32)
    (local $mapEnd i32)
    (local $counter i32)
    (local $pxCur i32)
    (local $imgBeg i32)
    (local $imgEnd i32)
    (local $cpyBeg i32)
    (local $cpyEnd i32)
    (local $idxCur i32)
    (local $idxEnd i32)
    (local $offset i32)
    (local $reservedPalette i32)

    (local $vec v128)
    (local $vecMask v128)

    (global.set $paletteCursor (i32.const 0))

    (local.set $imgBeg
      (local.set $imgEnd
        (local.set $cpyBeg
          (local.set $cpyEnd
            (call $initImgData (local.get $imgLength)(local.get $flag))))))

    (if (TEST($flag, SORTFLAG)) (then
      (call $pixelQsort
        (local.get $imgBeg)
        (local.get $imgEnd)
        (i32.const 0xffffffff))
    ))

    (if (TEST($flag, TRANSPARENTFLAG)) (then
      (local.set $imgBeg
        (call $excludeTransparent
          (local.get $imgBeg)
          (local.get $imgEnd)))
      (local.set $bgColor
        (i32.and
          (local.get $bgColor)
          (i32.const 0xffffff)))
    ))

    (local.set $reservedPalette (i32.ge_s (local.get $bgColor)(i32.const 0)))

    (if (TEST($flag, COUNTFLAG)) (then
      (local.set $imgEnd
        (call $makePixelSet
          (local.get $imgBeg)
          (local.get $imgEnd)
          (local.get $bitDepth)))
      (call $averageCut
        (local.get $imgBeg)
        (local.get $imgEnd)
        (local.get $bitDepth)
        (local.get $reservedPalette))
    )(else
      (call $medianCut
        (local.get $imgBeg)
        (local.get $imgEnd)
        (local.get $bitDepth)
        (local.get $reservedPalette))
    ))

    (if (local.get $reservedPalette)(then
      (i32.store8 offset=0 (i32.const 0)(i32.and (local.get $bgColor)(i32.const 0xff)))
      (local.set $bgColor (i32.shr_u (local.get $bgColor)(i32.const 8)))
      (i32.store8 offset=1 (i32.const 0)(i32.and (local.get $bgColor)(i32.const 0xff)))
      (local.set $bgColor (i32.shr_u (local.get $bgColor)(i32.const 8)))
      (i32.store8 offset=2 (i32.const 0)(i32.and (local.get $bgColor)(i32.const 0xff)))
    ))

    (if (TEST($flag, DITHERFLAG)) (then
      (call $dithering
        (i32.const WORKMEMSIZE)
        (local.get $cpyBeg)
        (local.get $cpyEnd)
        (local.get $width)
        (local.get $bitDepth))
    )(else
      (call $makeIdxImg
        (i32.const WORKMEMSIZE)
        (local.get $cpyBeg)
        (local.get $cpyEnd)
        (local.get $bitDepth))
    ))
  )
)