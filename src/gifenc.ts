import GIFWorker from "./worker?worker&inline";
import { EncodeTask, EncodeDeliverable } from "./worker";

import * as Block from "./block-maker";
import type { FrameControl } from "./block-maker";

interface WorkerHander {
  instance: Worker;
  stack: number;
}

/**
 * see gif spec
 */
type DisposalMethod = "none" | "not" | "background" | "previous";

interface OptionBase {
  /**
   * transferring data to workers
   * @default true
   */
  transfer?: boolean;

  /**
   * see gif spec
   * @default 0
   */
  xOffset?: number;

  /**
   * see gif spec
   * @default 0
   */
  yOffset?: number;

  /**
   * see gif spec
   * @default "none"
   */
  disposal?: DisposalMethod;
}

interface PushOption extends OptionBase {
  /**
   * frame local RGB palette
   * @default null
   */
  palette?: ArrayBuffer | null;

  /**
   * see gif spec
   * @default null
   */
  transparentIndex?: number | null;
}

interface PushImgOption extends OptionBase {
  /**
   * hint indicating than num of colors used is less than palette size
   * @default false
   */
  mayFitOnPalette?: boolean;

  /**
   * transparent when alpha ch is less than 0x80
   * @default false
   */
  useTransparent?: boolean;

  /**
   * apply dithering
   * @default false
   */
  applyDither?: boolean;

  /**
   * RGB of background color
   * @default null
   */
  bgColor?: ArrayLike<number> | null;
}

interface PushResult {
  /**
   * processed frame num
   */
  frameNum: number;

  /**
   * total frames processed
   */
  numProcessed: number;
}

interface Frame {
  data: ArrayBuffer;
  control: FrameControl;
}

interface Task {
  control: FrameControl;
  desc: EncodeTask;
  transfer: Transferable[];
}

function assertNum(name: string, num: number, min: number, max: number) {
  if (num < min || max < num) {
    throw new Error(`invalid ${name} (must be: ${min} <= ${name} <= ${max})`);
  }
}

function assertPalette(palette: ArrayBuffer, bitDepth: number) {
  if (palette.byteLength != (1 << bitDepth) * 3) {
    throw new Error(
      `invalid palette size (must be: ${(1 << bitDepth) * 3}byte)`
    );
  }
}

const DISPOSAL: { [key in DisposalMethod]: number } = {
  none: 0,
  not: 1,
  background: 2,
  previous: 3,
};

/**
 * gif encoder
 */
class Encoder {
  #width: number;
  #height: number;
  #bitDepth: number;
  #globalPalette: ArrayBuffer | null;
  #workers: WorkerHander[];
  #frameCallbacks: Map<number, (ret: EncodeDeliverable) => void>;
  #frames: Promise<Frame>[];
  #done: number;

  /**
   *
   * @param width - gif image width
   * @param height - gif image height
   * @param bitDepth - palette size
   * @param workerCount - num of workers use
   * @param globalPalette - global RGB palette (default: null)
   */
  constructor(
    width: number,
    height: number,
    bitDepth: number,
    workerCount: number,
    globalPalette: ArrayBuffer | null = null
  ) {
    assertNum("width", width, 1, 0xffff);
    assertNum("height", height, 1, 0xffff);
    assertNum("bitDepth", bitDepth, 1, 8);
    assertNum("workerCount", workerCount, 1, 0xffff);
    if (globalPalette) assertPalette(globalPalette, bitDepth);

    this.#width = width;
    this.#height = height;
    this.#bitDepth = bitDepth;
    this.#globalPalette = globalPalette;
    this.#workers = Array.from(
      { length: workerCount },
      (_, id): WorkerHander => {
        const instance = new GIFWorker();
        instance.onmessage = (msg) => this.#onmessage(id, msg);
        return { instance, stack: 0 };
      }
    );
    this.#frameCallbacks = new Map();
    this.#frames = [];
    this.#done = 0;
  }

  /**
   * width
   */
  get width(): number {
    return this.#width;
  }

  /**
   * height
   */
  get height(): number {
    return this.#height;
  }

  /**
   * bit depth
   */
  get bitDepth(): number {
    return this.#bitDepth;
  }

  /**
   * palette size
   */
  get paletteSize(): number {
    return 1 << this.#bitDepth;
  }

  #sendTask(task: Task): Promise<PushResult> {
    task.desc.frameNum = this.#frames.length;
    const freeWorker = this.#workers.reduce((accm, cur) =>
      accm.stack <= cur.stack ? accm : cur
    );
    ++freeWorker.stack;
    freeWorker.instance.postMessage(task.desc, task.transfer);
    const cb = new Promise<PushResult>((outerResolve) => {
      this.#frames.push(
        new Promise<Frame>((innerResolve) => {
          this.#frameCallbacks.set(task.desc.frameNum, (ret) => {
            outerResolve({
              frameNum: task.desc.frameNum,
              numProcessed: ++this.#done,
            });
            const data = ret.data;
            const control = task.control;
            if (control.palette === null && ret.palette !== null) {
              control.palette = ret.palette;
            }
            innerResolve({ data, control });
          });
        })
      );
    });
    return cb;
  }

  /**
   * push indices
   * @param indices - indices array (1index per byte)
   * @param width - image width
   * @param delayTick - delay time (1tick = 1/100sec)
   * @param opts - options
   * @returns - processed frame num and total frames processed
   */
  push(
    indices: ArrayBuffer,
    width: number,
    delayTick: number,
    {
      transfer = true,
      xOffset = 0,
      yOffset = 0,
      disposal = "none",
      palette = null,
      transparentIndex = null,
    }: PushOption = {}
  ): Promise<PushResult> {
    assertNum("width", width, 1, this.#width);
    assertNum("delayTick", delayTick, 0, 0xffff);
    const height = Math.floor(indices.byteLength / width);
    assertNum("height", height, 1, this.#height);
    assertNum("xOffset", xOffset, 0, this.#width - width);
    assertNum("yOffset", yOffset, 0, this.#height - height);
    if (DISPOSAL[disposal] === undefined) {
      throw new Error(`invalid disposal method "${disposal}"`);
    }
    if (palette) {
      assertPalette(palette, this.#bitDepth);
    } else if (!this.#globalPalette) {
      throw new Error("no palette available");
    }
    if (transparentIndex) {
      assertNum("transparentIndex", transparentIndex, 0, this.paletteSize * 3);
    }
    return this.#sendTask({
      control: {
        width,
        height,
        delayTick,
        xOffset,
        yOffset,
        palette,
        disposal: DISPOSAL[disposal],
        bitDepth: this.#bitDepth,
        transparentIndex,
      },
      desc: {
        order: {
          type: "index",
          data: indices,
          bitDepth: this.#bitDepth,
        },
        frameNum: 0,
      },
      transfer: transfer ? [indices] : [],
    });
  }

  /**
   * push image data
   * @param img - image data
   * @param delayTick - delay time (1tick = 1/100sec)
   * @param opts - options
   * @returns - processed frame num and total frames processed
   */
  pushImg(
    img: ImageData,
    delayTick: number,
    {
      transfer = true,
      xOffset = 0,
      yOffset = 0,
      disposal = "none",
      mayFitOnPalette = false,
      useTransparent = false,
      applyDither = false,
      bgColor = null,
    }: PushImgOption = {}
  ): Promise<PushResult> {
    assertNum("width", img.width, 1, this.#width);
    assertNum("delayTick", delayTick, 0, 0xffff);
    assertNum("height", img.height, 1, this.#height);
    assertNum("xOffset", xOffset, 0, this.#width - img.width);
    assertNum("yOffset", yOffset, 0, this.#height - img.height);
    if (DISPOSAL[disposal] === undefined) {
      throw new Error(`invalid disposal method "${disposal}"`);
    }
    if (bgColor) {
      if (bgColor.length < 3) {
        throw new Error("invalid bgColor");
      }
      if (
        img.width === this.#width &&
        img.height === this.#height &&
        !useTransparent
      ) {
        console.warn("bgColor is not used");
      }
    }
    const bgColorNum = bgColor
      ? (bgColor[0] & 0xff) |
        ((bgColor[1] & 0xff) << 8) |
        ((bgColor[2] & 0xff) << 16)
      : 0x80808080;

    return this.#sendTask({
      control: {
        width: img.width,
        height: img.height,
        delayTick,
        xOffset,
        yOffset,
        palette: null,
        disposal: DISPOSAL[disposal],
        bitDepth: this.#bitDepth,
        transparentIndex: useTransparent ? 0 : null,
      },
      desc: {
        order: {
          type: "image",
          data: img.data.buffer,
          bitDepth: this.#bitDepth,
          width: img.width,
          mayFitOnPalette,
          useTransparent,
          applyDither,
          bgColor: bgColorNum,
        },
        frameNum: 0,
      },
      transfer: transfer ? [img.data.buffer] : [],
    });
  }

  /**
   * generate gif image
   * @param loop - loop count (if 0 then endless) (default: 0)
   * @returns - gif image blob
   */
  async finish(loop: number = 0): Promise<Blob> {
    if (this.#frames.length === 0) {
      throw new Error("no frames");
    }
    assertNum("loop", loop, 0, 0xffff);

    const frames = await Promise.all(this.#frames);

    const buf: BlobPart[] = [];
    buf.push(
      Block.makeGlobalHeader(
        this.#width,
        this.#height,
        this.#bitDepth,
        0,
        this.#globalPalette
      )
    );
    if (frames.length > 1) {
      buf.push(Block.makeLoopControl(loop));
    }
    for (let i = 0; i < frames.length; ++i) {
      buf.push(
        Block.makeGraphicControlExtension(frames[i].control),
        Block.makeImageBlockHeader(frames[i].control),
        frames[i].data
      );
    }
    buf.push(new Uint8Array([0x3b]).buffer); // trailer
    return new Blob(buf, { type: "image/gif" });
  }

  /**
   * dispose instance
   */
  dispose() {
    this.#workers.forEach((handler) => handler.instance.terminate());
    this.#width = 0;
    this.#height = 0;
    this.#bitDepth = 0;
    this.#workers = [];
    this.#frameCallbacks = new Map();
    this.#frames = [];
    this.#done = 0;
  }

  #onmessage(id: number, msg: MessageEvent<EncodeDeliverable>) {
    const frameNum = msg.data.frameNum;
    this.#frameCallbacks.get(frameNum)!(msg.data);
    this.#frameCallbacks.delete(frameNum);
    --this.#workers[id].stack;
  }
}

export default Encoder;
export type { PushOption, PushImgOption, PushResult };
