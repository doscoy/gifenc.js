/// <reference types="vite/client" />

declare module "*.wat" {
  const moduleConstructor: { new(): WebAssembly.Instance };
  export default moduleConstructor;
}

declare module "*.wat?promise" {
  const moduleCompiler: (
  ) => Promise<WebAssembly.Instance>;
  export default moduleCompiler;
}