import WASMInit from "./gif-util.wat";

interface OrderBase {
  type: string;
  data: ArrayBuffer;
  bitDepth: number;
}

interface EncodeIndexOrder extends OrderBase {
  type: "index";
}

interface EncodeImageOrder extends OrderBase {
  type: "image";
  width: number;
  mayFitOnPalette: boolean;
  useTransparent: boolean;
  applyDither: boolean;
  bgColor: number;
}

interface EncodeTask {
  frameNum: number;
  order: EncodeIndexOrder | EncodeImageOrder;
}

interface EncodeResult {
  data: ArrayBuffer;
  palette: ArrayBuffer | null;
}

interface EncodeDeliverable extends EncodeResult {
  frameNum: number;
}

const WASM = (() => {
  const instance = new WASMInit();
  return {
    inputOffset: (instance.exports.inputOffset as WebAssembly.Global).value,
    countFlag: (instance.exports.countFlag as WebAssembly.Global).value,
    transparentFlag: (instance.exports.transparentFlag as WebAssembly.Global)
      .value,
    ditherFlag: (instance.exports.ditherFlag as WebAssembly.Global).value,
    memory: instance.exports.memory as WebAssembly.Memory,
    calcPalette: instance.exports.calcPalette as (
      byteLength: number,
      width: number,
      bitDepth: number,
      bgColor: number,
      flag: number
    ) => void,
    compress: instance.exports.compress as (
      byteLength: number,
      bitDepth: number
    ) => number,
  };
})();

function attachBuffer(input: ArrayBuffer) {
  const reqMem = WASM.inputOffset + input.byteLength;
  if (WASM.memory.buffer.byteLength < reqMem) {
    const sub = reqMem - WASM.memory.buffer.byteLength;
    const pages = Math.ceil(sub / 65536);
    WASM.memory.grow(pages);
  }
  new Uint8Array(WASM.memory.buffer, WASM.inputOffset, input.byteLength).set(
    new Uint8Array(input)
  );
}

function convGIFImgData({
  data,
  bitDepth,
  width,
  mayFitOnPalette,
  useTransparent,
  applyDither,
  bgColor,
}: EncodeImageOrder): EncodeResult {
  attachBuffer(data);
  WASM.calcPalette(
    data.byteLength,
    width,
    bitDepth,
    bgColor,
    (mayFitOnPalette ? WASM.countFlag : 0) |
      (useTransparent ? WASM.transparentFlag : 0) |
      (applyDither ? WASM.ditherFlag : 0)
  );
  const palette = WASM.memory.buffer.slice(0, (1 << bitDepth) * 3);
  const indexLen = data.byteLength / 4;
  const writeLen = WASM.compress(indexLen, bitDepth);
  const outputOffset = WASM.inputOffset + indexLen;
  const output = WASM.memory.buffer.slice(
    outputOffset,
    outputOffset + writeLen
  );
  return { data: output, palette };
}

function makeGIFImgData({ data, bitDepth }: EncodeIndexOrder): EncodeResult {
  attachBuffer(data);
  const writeLen = WASM.compress(data.byteLength, bitDepth);
  const outputOffset = WASM.inputOffset + data.byteLength;
  const output = WASM.memory.buffer.slice(
    outputOffset,
    outputOffset + writeLen
  );
  return { data: output, palette: null };
}

onmessage = (msg: MessageEvent<EncodeTask>) => {
  const frameNum = msg.data.frameNum;
  switch (msg.data.order.type) {
    case "index":
      postMessage({
        frameNum,
        ...makeGIFImgData(msg.data.order),
      } as EncodeDeliverable);
      break;
    case "image":
      postMessage({
        frameNum,
        ...convGIFImgData(msg.data.order),
      } as EncodeDeliverable);
      break;
  }
};

export type { EncodeTask, EncodeDeliverable };
