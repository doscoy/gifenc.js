import { defineConfig } from "vite";
import { resolve } from "path";
import esbuild from 'esbuild';

import Inspect from "vite-plugin-inspect";
import dts from "vite-plugin-dts";
import wat2wasm from "./plugin/wat2wasm";

function forceMinify(entry: string [], outfile: string) {
  return {
    name: "forceminify",
    closeBundle: () => {
      esbuild.buildSync({
        entryPoints: entry,
        minify: true,
        allowOverwrite: true,
        outfile: outfile
      })
    }
  }
}

export default defineConfig({
  plugins: [
    Inspect(),
    wat2wasm({ forceOptimize: false, enableDebugLog: false }),
    dts({ include: resolve(__dirname, "src/gifenc.ts") }),
    forceMinify([resolve(__dirname, "dist/gifenc.js")], "dist/gifenc.js"),
  ],
  worker: { plugins: [wat2wasm()] },
  build: {
    lib: {
      name: "gifenc.js",
      entry: resolve(__dirname, "src/gifenc.ts"),
      fileName: "gifenc",
    },
  },
});
