import { testGifEncoder, FLAGS } from "./test-gif";

const begBitDepth: number = 1;
const endBitDepth: number = 8;
const repeat: number = 64;

interface Src {
  path: string;
  flag: number;
}

function makeSrc(path: string, flag: number = 0): Src {
  return {
    path,
    flag,
  };
}

const srcList = [
  makeSrc("./rainbow1_h.png"),
  makeSrc("./rainbow1_v.png"),
  makeSrc("./rainbow2_h.png"),
  makeSrc("./rainbow2_v.png"),
  makeSrc("./noise.png"),
  makeSrc("./mandril.png"),
  makeSrc("./lena.png"),
  makeSrc("./lena_8801.png", FLAGS.COUNT),
  makeSrc("./lena_t.png", FLAGS.TRANSPARENT),
  makeSrc("./lena.png", FLAGS.DITHER),
];

async function check(srcIdx: number, bitDepth: number) {
  const src = srcList[srcIdx];
  const ret = await testGifEncoder(src.path, bitDepth, src.flag, repeat);
  const container = document.body.appendChild(document.createElement("div"));
  const canvas = container.appendChild(document.createElement("canvas"));
  canvas.width = ret.width;
  canvas.height = ret.height;
  canvas
    .getContext("bitmaprenderer")!
    .transferFromImageBitmap(ret.transferToImageBitmap());
  container.appendChild(document.createElement("hr")).scrollIntoView();

  ++bitDepth;
  if (bitDepth > endBitDepth) {
    ++srcIdx;
    bitDepth = begBitDepth;
  }
  if (srcIdx < srcList.length) setTimeout(check, 0, srcIdx, bitDepth);
}

setTimeout(check, 0, 0, begBitDepth);
