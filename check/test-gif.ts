import Init from "../src/gif-util.wat?promise";

const WASM = await (async () => {
  const instance = await Init();
  return {
    imgOffset: (instance.exports.inputOffset as WebAssembly.Global).value,
    countFlag: (instance.exports.countFlag as WebAssembly.Global).value,
    transparentFlag: (instance.exports.transparentFlag as WebAssembly.Global)
      .value,
    ditherFlag: (instance.exports.ditherFlag as WebAssembly.Global).value,
    memory: instance.exports.memory as WebAssembly.Memory,
    calcPalette: instance.exports.calcPalette as Function,
    compress: instance.exports.compress as Function,
  };
})();

const FLAGS = {
  COUNT: WASM.countFlag,
  TRANSPARENT: WASM.transparentFlag,
  DITHER: WASM.ditherFlag,
}

async function imageURL2ImageData(url: string) {
  const img = new Image();
  const loadEnd = new Promise<void>((resolve) => {
    img.addEventListener("load", () => resolve());
  });
  img.src = url;
  await loadEnd;

  const canvas = new OffscreenCanvas(img.naturalWidth, img.naturalHeight);
  const ctx = canvas.getContext("2d")!;
  ctx.drawImage(img, 0, 0);
  return ctx.getImageData(0, 0, canvas.width, canvas.height);
}

function encode(
  imgData: ImageData,
  bitDepth: number,
  flag: number,
  repeat: number
): [indexArr: Uint8Array, paletteArr: Uint8Array, log: string] {
  const outputOffset = WASM.imgOffset + imgData.data.byteLength;
  if (WASM.memory.buffer.byteLength < outputOffset) {
    const pages = Math.ceil(
      (outputOffset - WASM.memory.buffer.byteLength) / 65536
    );
    WASM.memory.grow(pages);
  }

  let idxBuf: ArrayBuffer = new ArrayBuffer(0);
  let pltBuf: ArrayBuffer = new ArrayBuffer(0);
  // @ts-ignore
  let dataBuf: ArrayBuffer = new ArrayBuffer(0);
  let rdcTime = 0;
  let cmpTime = 0;
  for (let i = 0; i < repeat; ++i) {
    const start = performance.now();
    const imgBuffer = new Uint8Array(
      WASM.memory.buffer,
      WASM.imgOffset,
      imgData.data.byteLength
    );
    imgBuffer.set(new Uint8Array(imgData.data.buffer));
    WASM.calcPalette(
      imgData.data.byteLength,
      imgData.width,
      bitDepth,
      0x80808080,
      flag
    );
    idxBuf = WASM.memory.buffer.slice(
      WASM.imgOffset,
      WASM.imgOffset + imgData.data.byteLength / 4
    );
    pltBuf = WASM.memory.buffer.slice(0, (1 << bitDepth) * 3);

    const rdcEnd = performance.now();
    const dataLen = WASM.compress(imgData.data.byteLength / 4, bitDepth);
    dataBuf = WASM.memory.buffer.slice(
      idxBuf.byteLength,
      idxBuf.byteLength + dataLen
    );
    const cmpEnd = performance.now();
    rdcTime += rdcEnd - start;
    cmpTime += cmpEnd - rdcEnd;
  }

  const idx = new Uint8Array(idxBuf);
  const palette = new Uint8Array(pltBuf);

  const time = {
    repeat,
    reduce: rdcTime / repeat,
    compress: cmpTime / repeat,
  };

  const flagText: string[] = [];
  if (flag & WASM.countFlag) flagText.push("count");
  if (flag & WASM.transparentFlag) flagText.push("transparent");
  if (flag & WASM.ditherFlag) flagText.push("dither");
  if (flagText.length === 0) flagText.push("no flag");
  const log = [
    `${imgData.width}x${imgData.height}px ${bitDepth}bit (${flagText.join(
      "|"
    )})`,
    `reduce: ${time.reduce.toFixed(3)}ms lzw: ${time.compress.toFixed(3)}ms`,
    `${(rdcTime + cmpTime).toFixed(3)}ms / ${repeat}`,
  ].join("\n");

  return [idx, palette, log];
}

function makePaletteImageData(
  width: number,
  height: number,
  bitDepth: number,
  paletteArr: Uint8Array
) {
  const paletteImg = new ImageData(width, height);
  const arr = paletteImg.data;
  for (let r = 0; r < paletteImg.height; ++r) {
    const paletteIdx =
      Math.floor((r * (1 << bitDepth)) / paletteImg.height) * 3;
    for (let c = 0; c < ((paletteImg.width / 6) | 0); ++c) {
      const i = (r * paletteImg.width + c) * 4;
      arr[i + 0] = paletteArr[paletteIdx + 0];
      arr[i + 1] = 0;
      arr[i + 2] = 0;
      arr[i + 3] = 255;
    }
    for (
      let c = (paletteImg.width / 6) | 0;
      c < ((paletteImg.width / 3) | 0);
      ++c
    ) {
      const i = (r * paletteImg.width + c) * 4;
      arr[i + 0] = 0;
      arr[i + 1] = paletteArr[paletteIdx + 1];
      arr[i + 2] = 0;
      arr[i + 3] = 255;
    }
    for (
      let c = (paletteImg.width / 3) | 0;
      c < ((paletteImg.width / 2) | 0);
      ++c
    ) {
      const i = (r * paletteImg.width + c) * 4;
      arr[i + 0] = 0;
      arr[i + 1] = 0;
      arr[i + 2] = paletteArr[paletteIdx + 2];
      arr[i + 3] = 255;
    }
    for (let c = (paletteImg.width / 2) | 0; c < paletteImg.width; ++c) {
      const i = (r * paletteImg.width + c) * 4;
      arr[i + 0] = paletteArr[paletteIdx + 0];
      arr[i + 1] = paletteArr[paletteIdx + 1];
      arr[i + 2] = paletteArr[paletteIdx + 2];
      arr[i + 3] = 255;
    }
  }
  return paletteImg;
}

function makeEncodedImageData(
  width: number,
  height: number,
  idxArr: Uint8Array,
  paletteArr: Uint8Array,
  transparentIndex: number = -1
) {
  const dstImg = new ImageData(width, height);
  for (let r = 0; r < dstImg.height; ++r) {
    for (let c = 0; c < dstImg.width; ++c) {
      const i = r * dstImg.width + c;
      const i4 = i * 4;
      const paletteIdx = idxArr[i] * 3;
      dstImg.data[i4 + 0] = paletteArr[paletteIdx + 0];
      dstImg.data[i4 + 1] = paletteArr[paletteIdx + 1];
      dstImg.data[i4 + 2] = paletteArr[paletteIdx + 2];
      dstImg.data[i4 + 3] = paletteIdx === transparentIndex ? 0 : 255;
    }
  }
  return dstImg;
}

async function testGifEncoder(
  src: string,
  bitDepth: number,
  flag: number,
  repeat: number
) {
  const srcImage = await imageURL2ImageData(src);
  const [idxArr, paletteArr, log] = encode(srcImage, bitDepth, flag, repeat);
  const paletteImage = makePaletteImageData(
    srcImage.width / 2,
    srcImage.height,
    bitDepth,
    paletteArr
  );
  const dstImage = makeEncodedImageData(
    srcImage.width,
    srcImage.height,
    idxArr,
    paletteArr,
    flag & WASM.transparentFlag ? 0 : -1
  );

  const canvas = new OffscreenCanvas(srcImage.width * 2.5, srcImage.height);
  const ctx = canvas.getContext("2d")!;
  ctx.putImageData(srcImage, 0, 0);
  ctx.putImageData(paletteImage, srcImage.width, 0);
  ctx.putImageData(dstImage, srcImage.width + paletteImage.width, 0);

  ctx.font = `16px monospace`;
  ctx.textAlign = "left";
  ctx.textBaseline = "top";
  ctx.fillStyle = "black";
  ctx.strokeStyle = "white";
  ctx.lineWidth = 4;
  let base = 8;
  log.split("\n").forEach((str) => {
    ctx.strokeText(str, 8, base);
    ctx.fillText(str, 8, base);
    base += 20;
  });

  return canvas;
}

export {testGifEncoder, FLAGS};